# README #
This repository provides a size resolved, temperature and oxygen dependent, n-component , marine snow sinking model.

This model is used in the Cram et al. (In Review) manuscript. "The role of particle size, ballast, temperature, and oxygen in the sinking flux to the deep sea."

These scripts were developed in MATLAB 2015, you will need a working copy of matlab to run this code.

"pvsto_lim3.m" is the one dimensional particle sinking model code
"particle_1d_simple.m" is a wrapper for pvsto_lim3.m that has sensible default parameters, (and thus fewer inputs)
"test_ballasted_sinking3.m" provides an example of how you would this 1d model and produces a few figures so one can see what is going on.

Dependencies can be found in the "tools" folder. If you are missing some files that appear to be necessary to run this, please contact the author. Some  matlab toolboxes may also be required.

### Who do I talk to? ###
The first author, Jacob Cram, can be contacted at cram at uw dot edu
You may also wish to direct questions to Curtis Deutsch in the School of Oceanography at University of Washington.