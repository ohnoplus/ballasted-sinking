function [MU] = waterviscosity(TR)
MU = 2.414e-5*10.^(248.8./(TR+133));
end