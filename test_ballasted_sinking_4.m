%% Paths

addpath('tools')

% Load in environment that seems to work
load('ballasted_sinking_environment.mat')
attach(basin_data)
clear('basin_data')
% regnames : eight ocean regions

% basin_profiles.T : Mean temperature profiles for each ocean
% region.

% basin_profiles.pdens : Pontential density profiles for each ocean
% region.

% bsn.mpcar : estemated mass percentage calcium carbonate in each
% region

% bsn.siFco : ibid, silicate
% bsn.CFco : ibid, carbon


% Settings

basinN = 4; % using profiles for sub tropical pacific
loc.alpha = 2.3; % fractal dimension of particles
%loc.Cr = 0.002;
loc.Cr = 0.2; % reminerilization rate
loc.Q10 = 2; % temperature dependence of reminerilizaiton rate
%loc.w1000 = 50;
% loc.w1000 = 5000; % sinking speed of 1 mm particle (m/d)
loc.rho_CaCO3 = 2.83; % density of carbonate
loc.rho_opal = 2.09; % density of silicate
loc.rho_POM = 1.033; % density of particulate organic matter
loc.c2pom = 0.1; % percentage of POM mass that contributed by carbon
loc.rho_std = 2.23; % standard particle density
loc.ds = 2; % smallest particle
loc.dl = 4000; % largist particle
loc.mode = 4; % allow for all temperature effects
z = 1:10:5100;
% test with ballast

loc.xmm = 20; % reference particle is 20um
loc.mxmm = 1; % mass of reference particle. Set to 1 since it
              % arbitrary.
loc.wxmm = 2; % sinking speed of refernce particle 2m/d
loc.KmOxy = 10; % Km for oxygen sensitivity.

%%
bp1 = pvsto_limw3(loc.alpha, 4.2, loc.alpha-1, [0 loc.Cr loc.Cr], ...
                 [loc.Q10, loc.Q10, loc.Q10],...
                 [loc.KmOxy,loc.KmOxy,loc.KmOxy],...
                 loc.xmm, loc.mxmm, loc.wxmm,...
                 loc.mode,loc.ds, loc.dl, ...
                 loc.rho_std, ...
                 [loc.rho_CaCO3 loc.rho_opal loc.rho_POM],...
                 [loc.rho_CaCO3 loc.rho_opal loc.rho_POM*loc.c2pom], ...
                 basin_profiles.pdens(:,basinN), 1.027, ...
                 [bsn.mpcar(basinN), bsn.mpsi(basinN),...
                  1-bsn.mpcar(basinN)-bsn.mpsi(basinN)],...
                 basin_profiles.T(:,basinN), ...
                 basin_profiles.O2(:,basinN), ...
                 z, 1000); 

figure(1);
clf
subplot(121)
plot(bp1.FZ(:,3), z) % flux profile POM
xlabel('Normalized Flux'); ylabel('Depth'); 
title('With ballast typical of STP')
set(gca, 'ydir', 'reverse');
ylim([0, 1000])

subplot(122)
plot(bp1.W, z) % sinking speed profile, note the speed maximum is
                % really shallow
xlabel('Speed (m/d)'); ylabel('Depth')
title('With ballast typical of STP')
set(gca, 'ydir', 'reverse');
ylim([0, 1000])

%% As above, but this time inside of a simplified wrapper function
%% with reasonable input values

bp1b = particle_1d_simple(loc.alpha, 4.2 , loc.alpha - 1, loc.Cr, loc.Q10,...
                         loc.KmOxy,...
                         bsn.mpcar(basinN), bsn.mpsi(basinN),...
                         basin_profiles.pdens(:,basinN),...
                         basin_profiles.T(:,basinN), ...
                         basin_profiles.O2(:,basinN), ...
                         z, 1000);

% Add on top of plot for more elaborate function.
figure(1);
subplot(121)
hold all;
plot(bp1b.FZ(:,3), z, 'o') % flux profile POM

subplot(122)
hold all;
plot(bp1b.W, z, 'o') % sinking speed profile, note the speed maximum is
                % really shallow

%% As above but this time particles are entirely un-ballasted (not
%% realistic)

% Here we replace the mass percentages of carbonate and silicate
% with zeros. Keep in mind these aren't standard particles, rather
% thy are only made of POM, which has density (1.0330g/ml) which is
% very similar to that of the surrounding water (1.024-1.028 in
% this average STP profile)

bp2 = particle_1d_simple(loc.alpha, 4.2 , loc.alpha - 1, ... 
                         loc.Cr, loc.Q10,...
                         loc.KmOxy,...
                         0,0,...
                         basin_profiles.pdens(:,basinN),...
                         basin_profiles.T(:,basinN), ...
                         basin_profiles.O2(:,basinN), ...
                         z, 1000);


figure(2);
clf
subplot(121)
plot(bp2.FZ(:,3), z) % flux profile POM
xlabel('Normalized Flux'); ylabel('Depth'); 
title('No Ballast')
set(gca, 'ydir', 'reverse');
ylim([0, 1000])

% the flux attenuates super quickly in this case

subplot(122)
plot(bp2.W, z)
xlabel('Speed (m/d)'); ylabel('Depth')
set(gca, 'ydir', 'reverse');
ylim([0, 1000])

%% DeVries Speed Max
% We can replicate the intermediate sinking speed maximum with
% un-ballasted particles, and low reminerilization
% There may be other solutions, but I don't want to exhaustively
% search for them.



uniform.pdens = repmat(mean(basin_profiles.pdens(:,basinN)),...
                       length(z), 1);
uniform.T = repmat(mean(basin_profiles.T(:,basinN)),...
                       length(z), 1);
uniform.O2 = repmat(mean(basin_profiles.O2(:,basinN)),...
                       length(z), 1);


bp3 = particle_1d_simple(loc.alpha, 4.2 , loc.alpha - 1, ... 
                         loc.Cr/100, loc.Q10,...
                         loc.KmOxy,...
                         0,0,...
                         basin_profiles.pdens(:,basinN),...
                         basin_profiles.T(:,basinN), ...
                         basin_profiles.O2(:,basinN), ...
                         z, 1000);


figure(3);
clf
subplot(121)
plot(bp3.FZ(:,3), z) % flux profile POM
xlabel('Normalized Flux'); ylabel('Depth'); 
title('Simulate DeVries Like Profiles')
set(gca, 'ydir', 'reverse');
set(gca, 'xscale', 'log')
ylim([0, 1000])

subplot(122)
plot(bp3.W, z) % sinking speed profile, note the speed maximum is
                % really shallow
xlabel('Speed (m/d)'); ylabel('Depth')
set(gca, 'ydir', 'reverse');
ylim([0, 1000])

%% Example of a one component particle, of standard, rather than
%% POM density

%%
bp4 = pvsto_limw3(loc.alpha, 4.2, loc.alpha-1, [loc.Cr], ...
                 [loc.Q10],...
                 [loc.KmOxy],...
                 loc.xmm, loc.mxmm, loc.wxmm,...
                 loc.mode,loc.ds, loc.dl, ...
                 loc.rho_std, ...
                 [loc.rho_std],...
                 [loc.rho_std], ...
                 basin_profiles.pdens(:,basinN), 1.027, ...
                 [loc.rho_std],...
                 basin_profiles.T(:,basinN), ...
                 basin_profiles.O2(:,basinN), ...
                 z, 1000); 

figure(1);
clf
subplot(121)
plot(bp4.FZ(:,1), z) % Normally I plot the flux of the third
                     % component (POM), but here there is only one component
xlabel('Normalized Flux'); ylabel('Depth'); 
title('One component particle')
set(gca, 'ydir', 'reverse');
ylim([0, 1000])

subplot(122)
plot(bp4.W, z) 
xlabel('Speed (m/d)'); ylabel('Depth')
set(gca, 'ydir', 'reverse');
ylim([0, 1000])